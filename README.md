# Preparation to fuck Moon Lord

## Weapon & Armor
[(傳奇) 泰拉刃](https://terraria-zh.gamepedia.com/%E6%B3%B0%E6%8B%89%E5%88%83)
[(Legendary) Terra Blade](https://terraria.gamepedia.com/Terra_Blade)

[(神級/無情) 吸血鬼刀](https://terraria-zh.gamepedia.com/%E5%90%B8%E8%A1%80%E9%AC%BC%E5%88%80)
[(Godly/Demonic) Vampire Knives](https://terraria.gamepedia.com/Vampire_Knives)

[甲蟲盔甲 (甲蟲殼)](https://terraria-zh.gamepedia.com/%E7%94%B2%E8%99%AB%E7%9B%94%E7%94%B2)
[Beetle armor (Beetle Shell)](https://terraria.gamepedia.com/Beetle_armor)

## Accessories (全護佑+4防)
[星星面纱](https://terraria-zh.gamepedia.com/%E6%98%9F%E6%98%9F%E9%9D%A2%E7%BA%B1)
[Star Veil](https://terraria.gamepedia.com/Star_Veil)

[神話護身符](https://terraria-zh.gamepedia.com/%E7%A5%9E%E8%AF%9D%E6%8A%A4%E8%BA%AB%E7%AC%A6)
[Charm of Myths](https://terraria.gamepedia.com/Charm_of_Myths)

[冰凍盾](https://terraria-zh.gamepedia.com/%E5%86%B0%E5%86%BB%E7%9B%BE)
[Frozen Turtle Shell](https://terraria.gamepedia.com/Frozen_Turtle_Shell)

[十字章護盾](https://terraria-zh.gamepedia.com/%E5%8D%81%E5%AD%97%E7%AB%A0%E6%8A%A4%E7%9B%BE)
[Ankh Shield](https://terraria.gamepedia.com/Ankh_Shield)

[天界殼](https://terraria-zh.gamepedia.com/%E5%A4%A9%E7%95%8C%E5%A3%B3)
[Celestial Shell](https://terraria.gamepedia.com/Celestial_Shell)

[泰拉閃耀靴](https://terraria-zh.gamepedia.com/%E6%B3%B0%E6%8B%89%E9%97%AA%E8%80%80%E9%9D%B4)
[Terraspark Boots](https://terraria.gamepedia.com/Terraspark_Boots)

[翅膀](https://terraria-zh.gamepedia.com/%E7%BF%85%E8%86%80)
[Wings](https://terraria.gamepedia.com/Wings)

## Potion
[再生藥水](https://terraria-zh.gamepedia.com/%E5%86%8D%E7%94%9F%E8%8D%AF%E6%B0%B4)
[Restoration Potion](https://terraria.gamepedia.com/Restoration_Potion)

[超級治療藥水](https://terraria-zh.gamepedia.com/%E8%B6%85%E7%BA%A7%E6%B2%BB%E7%96%97%E8%8D%AF%E6%B0%B4)
[Super Healing Potion](https://terraria.gamepedia.com/Super_Healing_Potion)

[鐵皮藥水](https://terraria-zh.gamepedia.com/%E9%93%81%E7%9A%AE%E8%8D%AF%E6%B0%B4)
[Ironskin Potion](https://terraria.gamepedia.com/Ironskin_Potion)

[黃金大餐](https://terraria-zh.gamepedia.com/Golden_Delight)
[Golden Delight](https://terraria.gamepedia.com/Golden_Delight)

[海鲜大餐](https://terraria-zh.gamepedia.com/%E6%B5%B7%E9%B2%9C%E5%A4%A7%E9%A4%90)
[Seafood Dinner](https://terraria.gamepedia.com/Fishing_foods)

## other
[黑色通牒 (海盜船)](https://terraria-zh.gamepedia.com/The_Black_Spot)
[The Black Spot (Pirate Ship)](https://terraria.gamepedia.com/The_Black_Spot)

[星塵之龍法杖](https://terraria-zh.gamepedia.com/%E6%98%9F%E5%B0%98%E4%B9%8B%E9%BE%99%E6%B3%95%E6%9D%96)
[Stardust Dragon Staff](https://terraria.gamepedia.com/Stardust_Dragon_Staff)

[手機](https://terraria-zh.gamepedia.com/%E6%89%8B%E6%9C%BA)
[Cell Phone](https://terraria.gamepedia.com/Cell_Phone)